#include "testvxuconvolve.h"
#include "nvxcu.h"
#include <NVX/nvx.h>
#include <string>
#include <fstream>
#include <iostream>


nvxcu_pitch_linear_image_t createImageU8(uint32_t width, uint32_t height, uint32_t pitch)
{
    void *dev_ptr = NULL;   
    nvxcu_pitch_linear_image_t image;
    image.base.image_type = NVXCU_PITCH_LINEAR_IMAGE;
    image.base.format = NVXCU_DF_IMAGE_U8;
    image.base.width = width;
    image.base.height = height;
    image.planes[0].dev_ptr = dev_ptr;
    image.planes[0].pitch_in_bytes = pitch;
    return image;
}

void testvxuconvolve(uint32_t width, uint32_t height, uint32_t roi_x, uint32_t roi_y, uint32_t roi_width, uint32_t roi_height)
{
    auto x_offset = (roi_y*width) + roi_x;

    size_t size = width * height;
    
    // host memory
    auto src = new unsigned char[size];
    auto blur = new unsigned char[size];
    auto absdiff = new unsigned char[size];

    // kernel 
    size_t kernel_width = 3;
    size_t kernel_height = 3;
    size_t kernel_size = kernel_width*kernel_height;
    
    auto kernel_ptr = new int16_t[kernel_size];
    memset(kernel_ptr, 0, kernel_size);
    kernel_ptr[4] = 1; // src == blur

    // device memory
    void *d_src, *d_blur, *d_absdiff;
	cudaMalloc(&d_src, size);
    cudaMalloc(&d_blur, size);
    cudaMalloc(&d_absdiff, size);
    
    void *kernel_dev_ptr;
    cudaMalloc(&kernel_dev_ptr, kernel_size);
    
    // reading image
    std::ifstream file("./1280x960.raw", std::ios::in | std::ios::binary | std::ios::ate);		
    size_t req_size = static_cast<size_t>(file.tellg());    
	file.seekg(0, std::ios::beg);
	file.read((char*)src, size);	
    file.close();
    
    cudaStream_t stream;
    cudaStreamCreate(&stream);
    
    // creating exec_target
    nvxcu_stream_exec_target_t exec_target;
    exec_target.base.exec_target_type = NVXCU_STREAM_EXEC_TARGET;
    exec_target.stream = stream;
    cudaGetDeviceProperties(&exec_target.dev_prop, 0);
    
    // kernel_dev_ptr
    vx_uint32 kernel_div = 1;    
    cudaMemcpyAsync(kernel_dev_ptr, kernel_ptr, kernel_size, cudaMemcpyHostToDevice, stream);
    cudaStreamSynchronize(stream);

    // border
    nvxcu_border_t border;
    border.mode = NVXCU_BORDER_MODE_REPLICATE;
    
    // temporary buffer size
    nvxcu_tmp_buf_size_t temp_buf_size = nvxcuConvolve_GetBufSize(roi_width, roi_height, NVXCU_DF_IMAGE_U8, kernel_width, kernel_height, NVXCU_DF_IMAGE_U8, &border, &exec_target.dev_prop);
    nvxcu_tmp_buf_t tmp_buf = {NULL, NULL};
    if (temp_buf_size.dev_buf_size > 0)
    {
        cudaMalloc(&tmp_buf.dev_ptr, temp_buf_size.dev_buf_size);
    }
    if (temp_buf_size.host_buf_size > 0)
    {
        cudaMallocHost(&tmp_buf.host_ptr, temp_buf_size.host_buf_size);
    }

    auto src_nvxcu = createImageU8(roi_width, roi_height, width);
    src_nvxcu.planes[0].dev_ptr = (void*)((unsigned char*)d_src + x_offset);
    auto blur_nvxcu = createImageU8(roi_width, roi_height, width);
    blur_nvxcu.planes[0].dev_ptr = (void*)((unsigned char*)d_blur + x_offset);
    auto absdiff_nvxcu = createImageU8(roi_width, roi_height, width);
    absdiff_nvxcu.planes[0].dev_ptr = (void*)((unsigned char*)d_absdiff + x_offset);
    
    cudaMemcpyAsync(d_src, src, size, cudaMemcpyHostToDevice, stream);
    cudaMemsetAsync(d_blur, 0, size, stream);
    cudaMemsetAsync(d_absdiff, 0, size, stream);
    
    auto res = nvxcuConvolve(&src_nvxcu.base, (const int16_t*)kernel_dev_ptr, kernel_div, kernel_width, kernel_height, &blur_nvxcu.base, &border, &tmp_buf, &exec_target.base);        
    nvxcuAbsDiff(&src_nvxcu.base, &absdiff_nvxcu.base, &absdiff_nvxcu.base, &exec_target.base);
    
    cudaStreamSynchronize(stream);

    cudaMemcpyAsync(blur, d_blur, size, cudaMemcpyDeviceToHost, stream);
    cudaMemcpyAsync(absdiff, d_absdiff, size, cudaMemcpyDeviceToHost, stream);
	cudaStreamSynchronize(stream);

    std::string hint = std::to_string(roi_x) + "_" + std::to_string(roi_y) + "_" + std::to_string(roi_width) + "_" + std::to_string(roi_height);
    std::cout << hint << "<>" << res << std::endl;
    {
        std::ofstream outFile("blur_" + hint + ".raw", std::ios::out | std::ios::binary);
        outFile.write((const char*)blur, size);
        outFile.close();
    }
    {
        std::ofstream outFile("absdiff_" + hint + ".raw", std::ios::out | std::ios::binary);
        outFile.write((const char*)absdiff, size);
        outFile.close();
    }
    
    cudaStreamDestroy(stream);

    if (temp_buf_size.dev_buf_size > 0)
    {
        cudaFree(tmp_buf.dev_ptr);
    }
    if (temp_buf_size.host_buf_size > 0)
    {
        cudaFreeHost(tmp_buf.host_ptr);
    }

	cudaFree(d_src);
    cudaFree(d_blur);

    cudaFree(kernel_dev_ptr);
    delete[] kernel_ptr;
    
    delete[] src;
	delete[] blur;
}

void runtests()
{
    testvxuconvolve(1280, 960, 0, 0, 1280, 960);
    testvxuconvolve(1280, 960, 0, 0, 512, 512);
    testvxuconvolve(1280, 960, 256, 256, 512, 512);
    testvxuconvolve(1280, 960, 0, 256, 512, 512);
    testvxuconvolve(1280, 960, 256, 0, 512, 512);
    testvxuconvolve(1280, 960, 512, 0, 512, 512);
    testvxuconvolve(1280, 960, 1024, 32, 128, 512);
    testvxuconvolve(1280, 960, 1024, 16, 128, 16);
    testvxuconvolve(1280, 960, 1024, 16, 128, 32);
    testvxuconvolve(1280, 960, 1024, 16, 128, 64);
}